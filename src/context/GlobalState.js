import React, { useReducer } from 'react';

import ShopContext from './shop-context';
import { shopReducer, ADD_PRODUCT, REMOVE_PRODUCT, SET_CART, DISCARD_PRODUCT } from './reducers';

const GlobalState = props => {
    const [cartState, dispatch] = useReducer(shopReducer, { cart: [], totalQuantity: 0, totalPrice: 0 });

    const addProductToCart = (product, quantity = 1) => {
        // setTimeout(() => {
        dispatch({ type: ADD_PRODUCT, product: product, quantity: quantity });
        // }, 700);
    };

    const removeProductFromCart = (productId, quantity = 1) => {
        // setTimeout(() => {
        dispatch({ type: REMOVE_PRODUCT, productId: productId, quantity: Math.abs(quantity) });
        // }, 700);
    };

    const setItemsToCart = cart => {
        dispatch({ type: SET_CART, cart: cart });
    }

    const discardProduct = (productId) => {
        dispatch({ type: DISCARD_PRODUCT, productId: productId });
    }

    return (
        <ShopContext.Provider
            value={{
                cart: cartState.cart,
                totalQuantity: cartState.totalQuantity,
                totalPrice: cartState.totalPrice,
                addProductToCart: addProductToCart,
                removeProductFromCart: removeProductFromCart,
                setItemsToCart: setItemsToCart,
                discardProduct: discardProduct
            }}
        >
            {props.children}
        </ShopContext.Provider>
    );
}

export default GlobalState;