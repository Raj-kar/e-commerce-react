export const ADD_PRODUCT = 'ADD_PRODUCT';
export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';
export const SET_CART = 'SET_CART';
export const DISCARD_PRODUCT = 'DISCARD_PRODUCT';

const addProductToCart = (product, quantity, state) => {
    const updatedCart = [...state.cart];
    const updatedItemIndex = updatedCart.findIndex(
        item => item.id === product.id
    );

    if (updatedItemIndex < 0) {
        updatedCart.push({ ...product, quantity: 1 });
    } else {
        const updatedItem = {
            ...updatedCart[updatedItemIndex]
        };
        updatedItem.quantity += quantity;
        updatedCart[updatedItemIndex] = updatedItem;
    }
    state.totalQuantity += quantity;
    state.totalPrice += (quantity * product.price);

    const newState = { ...state, cart: updatedCart };
    localStorage.setItem('cart', JSON.stringify(newState));
    return newState;
};

const removeProductFromCart = (productId, quantity, state) => {
    // console.log('Removing product with id: ' + productId);
    const updatedCart = [...state.cart];
    const updatedItemIndex = updatedCart.findIndex(item => item.id === productId);

    state.totalQuantity -= quantity;
    state.totalPrice -= (quantity * updatedCart[updatedItemIndex].price);

    const updatedItem = {
        ...updatedCart[updatedItemIndex]
    };
    updatedItem.quantity -= quantity;
    if (updatedItem.quantity <= 0) {
        updatedCart.splice(updatedItemIndex, 1);
    } else {
        updatedCart[updatedItemIndex] = updatedItem;
    }

    const newState = { ...state, cart: updatedCart };
    localStorage.setItem('cart', JSON.stringify(newState));
    return newState;
};

const setCartState = (cartData, state) => {
    const updatedCart = [...state.cart];
    updatedCart.push(...cartData.cart);
    return { ...state, cart: updatedCart, totalPrice: cartData.totalPrice, totalQuantity: cartData.totalQuantity };
}

const discardProduct = (productId, state) => {
    const updatedCart = [...state.cart];
    const updatedItem = updatedCart.find(item => item.id === productId);

    state.totalQuantity -= updatedItem.quantity;
    state.totalPrice -= (updatedItem.quantity * updatedItem.price);

    const updatedState = { ...state, cart: state.cart.filter(item => item.id !== productId) };

    localStorage.setItem('cart', JSON.stringify(updatedState));
    return updatedState;
}

export const shopReducer = (state, action) => {
    switch (action.type) {
        case ADD_PRODUCT:
            return addProductToCart(action.product, action.quantity, state);
        case REMOVE_PRODUCT:
            return removeProductFromCart(action.productId, action.quantity, state);
        case SET_CART:
            return setCartState(action.cart, state);
        case DISCARD_PRODUCT:
            return discardProduct(action.productId, state);
        default:
            return state;
    }
};