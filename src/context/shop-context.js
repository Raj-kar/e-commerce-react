import React from 'react';

export default React.createContext({
    cart: [],
    totalQuantity: 0,
    totoalPrice: 0,
    addProductToCart: (product, quantity) => { },
    removeProductFromCart: (productId, quantity) => { },
    setItemsToCart: cart => { },
    discardProduct: productId => { },
});