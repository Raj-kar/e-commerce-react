import React, { useContext, useRef, useState } from 'react';
import './Cart.scss';
import cancelLogo from '../assets/cancel.png';
import ShopContext from '../../context/shop-context';

const Cart = ({ item }) => {
    const [quantity, setQuantity] = useState(item.quantity);

    const inputRef = useRef();

    const cart = useContext(ShopContext);

    const addItem = () => {
        cart.addProductToCart(item);
        setQuantity(p => p + 1);
    };

    const removeItem = () => {
        cart.removeProductFromCart(item.id);
        setQuantity(p => p - 1);
    };

    const handleCartInput = event => {
        setQuantity(event.target.value);
    };

    const onBlurHandler = event => {
        if (event.target.value === '') {
            inputRef.current.value = item.quantity;
        } else {
            const newQuantity = inputRef.current.value;
            if (newQuantity > item.quantity) {
                if (newQuantity - item.quantity === 1)
                    cart.addProductToCart(item);
                else cart.addProductToCart(item, newQuantity - item.quantity);
            } else {
                if (newQuantity - item.quantity === -1)
                    cart.removeProductFromCart(item.id);
                else
                    cart.removeProductFromCart(
                        item.id,
                        newQuantity - item.quantity
                    );
            }
        }
    };

    const onDiscardProduct = () => {
        cart.discardProduct(item.id);
    };

    return (
        <div className="cart">
            <img src={item.thumbnail} alt="keyboard" />
            <h3>{item.name}</h3>
            <h3>${item.price * item.quantity}</h3>
            <div className="cart__quantity">
                <button onClick={removeItem}>-</button>
                <input
                    ref={inputRef}
                    type="number"
                    name="quantity"
                    id="quantity"
                    value={quantity}
                    onChange={handleCartInput}
                    onBlur={onBlurHandler}
                />
                <button onClick={addItem}>+</button>
            </div>
            <button onClick={onDiscardProduct} className="discard">
                <img src={cancelLogo} alt="close" title="discard item" />
            </button>
        </div>
    );
};

export default Cart;
