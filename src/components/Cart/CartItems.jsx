import React, { useContext } from 'react';
import shopContext from '../../context/shop-context';
import Cart from './Cart';
import "./CartItems.scss";

const CartItems = () => {
    const cart = useContext(shopContext);

    return (
        <div className="cart-items">
            <h1 className="text-huge">Checkout</h1>
            <div className="cart-container">
                <div className="cart-container__headings">
                    <span>Image</span>
                    <span>Name</span>
                    <span>Price</span>
                    <span>Quantity</span>
                    <span>Discard</span>
                </div>
                {cart.totalQuantity > 0 ? cart.cart.map(item => <Cart item={item} key={item.id} />) : <h1 className="alert-text">Add Some Items to your Cart</h1>}
            </div>

        </div>
    )
}

export default CartItems;
