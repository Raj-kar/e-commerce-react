import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';

import ShopContext from '../../context/shop-context';

import "./Navbar.scss";

const Navbar = () => {
    const cart = useContext(ShopContext);   

    useEffect(() => {
        const cartItems = localStorage.getItem('cart');
        cartItems && cart.setItemsToCart(JSON.parse(cartItems));
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <nav className="nav">
            <header>
                <ul className="nav__list">
                    <li className="nav__item">
                        <Link to="/">Home</Link>
                    </li>
                    <Link to='/cart'>
                        <div className="nav__cart">
                            <li className="nav__item nav__item-count">{cart.totalQuantity}</li>
                            {cart.totalQuantity > 0 && <li className="nav__item nav__item-price">${cart.totalPrice}</li>}
                        </div>
                    </Link>
                </ul>
            </header>
        </nav>
    )
}

export default Navbar;
