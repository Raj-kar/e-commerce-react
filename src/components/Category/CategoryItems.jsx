import React from 'react';
import { Link } from 'react-router-dom';
import { categories } from '../../data/helper';
import Category from './Category';
import "./CategoryItems.scss";

const CategoryItems = () => {
    return (
        <div className="categories">
            {categories.map(categorie => (
                <Link to={`/${categorie.id}`} key={categorie.id} >
                    <Category title={categorie.name} />
                </Link>
            ))}
        </div>
    )
}

export default CategoryItems;
