import React from 'react';
import "./Category.scss";

const Category = ({ title }) => {
    return (
        <div className="card">
            <h1>{title || 'keyboard'}</h1>
        </div>
    )
}

export default Category;
