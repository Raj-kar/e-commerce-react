import React, { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router';
import "./ItemList.scss";
import { FILTER_OPTIONS, getCategoryName, products as PRODUCTS } from '../../data/helper';
import Item from './Item';
import ItemFilters from './ItemFilters';




const ItemList = () => {
    const { categoryId } = useParams();
    const [products, setProducts] = useState([]);
    

    const filterProduct = useCallback(() => {
        const filteredProducts = PRODUCTS.filter(product => product.categoryId === categoryId);
        setProducts(filteredProducts);
    }, [categoryId]);

    useEffect(() => {
        filterProduct();
    }, [filterProduct]);

    const handleFilterItems = (filterState) => {
        let filteredProducts;

        for (let i = 0; i < filterState.length; i++) {
            if (filterState[i]) {
                const filter = FILTER_OPTIONS[i];
                filteredProducts = products.filter(product => product[filter.type] >= filter.value);
                setProducts(filteredProducts);
            }
        }

        filteredProducts ? setProducts(filteredProducts) : filterProduct();
    }

    return (
        <div className="container">
            <ItemFilters onHandleFilter={handleFilterItems} />

            <div>
                <h1 className="text-huge">{getCategoryName(categoryId)}</h1>
                <div className="products">
                    {products?.length === 0 && <h1>Fetching .....</h1>}
                    {products?.map(product => (
                        <Item key={product.id} product={product} />
                    ))}
                </div>
            </div>
        </div>
    )
}

export default ItemList;
