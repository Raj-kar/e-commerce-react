import React, { useContext } from 'react';
import shopContext from '../../context/shop-context';
import "./Item.scss";

const Item = ({ product }) => {
    const { id, name, price, delivery, thumbnail, inStock } = product;
    const cart = useContext(shopContext);

    return (
        <div className="product">
            <div className="product__thumbnail">
                <img src={thumbnail} alt={id} className="product__thumbnail-img" />
            </div>
            <div className="product__footer">
                <h1>{name} </h1>
                <h2>${price}</h2>
                {inStock ? <p className="green">In Stock</p> : <p className="red">Out of Stock</p>}
            </div>
            <button disabled={delivery} onClick={() => cart.addProductToCart(product)}>Add to Cart</button>
        </div>
    );
}

export default Item;
