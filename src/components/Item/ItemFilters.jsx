import React, { useState } from 'react';
import "./ItemFilters.scss";
import { FILTER_OPTIONS as OPTIONS } from '../../data/helper';

const transformText = (text) => text.charAt(0).toUpperCase() + text.slice(1);

const ItemFilters = ({ onHandleFilter }) => {
    const [checkedState, setCheckedState] = useState(
        new Array(OPTIONS.length).fill(false)
    );

    const onFiltered = (position) => {
        const updatedCheckedState = checkedState.map((item, index) =>
            index === position ? !item : item
        );

        setCheckedState(updatedCheckedState);
        onHandleFilter(updatedCheckedState);
    }

    return (
        <div className="filters">
            <h1 className="text-huge">Filter</h1>
            {OPTIONS.map((option, index) => (
                <div className="filters__group" key={option.type}>
                    <input type="checkbox" id={option.type} name={option.type} value={option.type} onChange={() => onFiltered(index)} checked={checkedState[index]} />
                    <label htmlFor={option.type}> {transformText(option.type)} </label>
                </div>
            ))}
        </div>
    );
}

export default ItemFilters;
