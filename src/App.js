import Navbar from './components/navbar/Navbar';
import "./App.scss";
import CategoryItems from './components/Category/CategoryItems';
import { Route, Switch } from 'react-router';
import ItemList from './components/Item/ItemList';
import CartItems from './components/Cart/CartItems';

const App = () => {
  return (
    <div className="main">
      <Navbar />
      <Switch>
        <Route path="/cart" exact>
          <CartItems />
        </Route>
        <Route path="/:categoryId" exact>
          <ItemList />
        </Route>
        <Route path='/'>
          <CategoryItems />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
